*** Settings ***
Library               RequestsLibrary

*** Test Cases ***

Quick GET Request Test
    ${response}=    Get    https://reqres.in/api/users/2    expected_status=200
    Log To Console    ${response}
    # Log To Console    ${response.text}
    # Log To Console    ${response.content}
    Log To Console    ${response.json()}
    Log To Console    ${response.status_code}

# Quick POST Request Test
#     &{data}=    Create Dictionary    name:Tonhom    job:Tester
#     ${response}=    Post    https://reqres.in/api/users    data=${data}    expected_status=201
#     Log To Console    ${response.status_code}
#     Log To Console    ${response.content}

# Quick PUT Request Test
#     &{data}=    Create Dictionary    name:KH Nipaphorn    job:Student
#     ${response}=    Put    https://reqres.in/api/users/2    data=${data}    expected_status=200
#     Log To Console    ${response.status_code}
#     Log To Console    ${response.content}

# Quick DELETE Request Test
#     ${response}=    Delete    https://reqres.in/api/users/2    expected_status=204
#     Log To Console    ${response.status_code}
#     Log To Console    ${response.content}



# Quick GET Request Test
#     [Tags]    get
#     ${response}=    Get    https://reqres.in/api/users/2    #expected_status=200
#     Status Should Be    OK    ${response}
#     Status Should Be    200


# Quick Get Request Test
#     ${response}=    GET  https://www.google.com

# Quick Get Request With Parameters Test
#     ${response}=    GET  https://www.google.com/search  params=query=ciao  expected_status=200

Quick Get A JSON Body Test
    ${response}=    GET  https://jsonplaceholder.typicode.com/posts/1
    ${title}    Set Variable    sunt aut facere repellat provident occaecati excepturi optio reprehenderit
    ${body}    Set Variable    quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto
    Should Be Equal As Strings    1  ${response.json()}[id]
    # Log To Console    ${response.json()}[title]
    Should Be Equal As Strings    ${title}    ${response.json()}[title]
    # Log To Console    ${response.json()}[body]
    Should Be Equal As Strings    ${body}    ${response.json()}[body]


