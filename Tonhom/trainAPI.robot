*** Settings ***
Library    SeleniumLibrary
Library    String
Library    DateTime
Library    BuiltIn
Library    OperatingSystem
Library    RequestsLibrary

*** Variable ***
${url}    http://uat.cubesofttech.com/
${BROWSER}    chrome
${text2}    Tonhom

*** Keywords ***
Go to page
    [Arguments]    ${username}    ${password}
    Open Browser    ${url}    ${BROWSER}
    Input Text    username    ${username}
    Input Password    password    ${password}
    Click button    id=submit myBtn
Show text
    Log To Console    ${text2}
*** Test Case ***
# TC001
#     [Template]    Go to page
#     test    test
#     test_it_roles    1234

# TC002    Log To Console    Robot Framework
#     Log    Robot Framework
#     ${text}    Set Variable    Test variable
#     Log To Console    ${text}
#     Show text

# TC003 Sub string
#     ${string}    Set Variable    RobotFramework
#     ${sub_string}=    Get Substring    ${string}    5
#     Log To Console    ${sub_string}

TC004 List
    @{list}    Create List    a    b    c
    ${scalar}    Create List    a    b    c
    ${ints}    Create List    ${1}    ${2}    ${3}
    Log To Console    ${list}
    Log To Console    ${scalar}
    Log To Console    ${ints}