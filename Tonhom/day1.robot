*** Settings ***
Library    BuiltIn
Library    String
Library    Collections

*** Variable ***
${textTC03}    TC03 Robot Framework from Variable
&{dictionary}    color=red    animal=dog
${TC09}    Test Documentation

@{name}    Nipaphorn    Khantikit
@{name2}    @{name}    Khanti
@{nothing}
@{many}    one    two    three
...        four    five    six

@{list}    apple    orange    mango

&{dictionary1}    a=aaa    b=bbb



*** Keywords ***
Show text Robot Framework
    Log To Console    TC04 This is Show text from keywords

Get Length of list Student
    [Arguments]    ${studentA}    ${studentB}    ${studentC}
    @{student}    Create List    ${studentA}    ${studentB}    ${studentC}
    ${length}    get length    ${student}
    ${length}    convert to string    ${length}
    [return]    ${length}

do plus
    [Arguments]    ${x}    ${y}
    ${sum}    Evaluate    ${x}+${y}
    Return From Keyword    ${sum}

for loop i
    [Arguments]    ${i}
    ${sum}    set variable    0
    FOR    ${j}    IN RANGE    0    10
        ${sum}    Evaluate    ${sum}+${i}*${j}
    END
    Return From Keyword    ${sum}

*** Test Case ***

# ----------- part 1 ------------ #
# TC001 log robot framework
#     Log To Console    TC01 Robotframework
# TC002 log robot framework with variable
#     ${text02}    Set Variable    TC02 Robot Framework with variable
#     Log To Console    ${text02}
# TC003 log robot framework from variable
#     Log To Console    ${textTC03}
# TC004 log robot framework from keywords
#     Show text Robot Framework

# ----------- part 2 ------------ #
# TC005 get time in BuiltIn library
#     ${time}=    Get Time 			
#     ${secs}=    Get Time    epoch 		
#     ${year}=    Get Time    return year 		
#     ${yyyy}    ${mm}    ${dd}=    Get Time    year,month,day
#     @{time2}=    Get Time    year month day hour min sec 		
#     ${y}    ${s}=    Get Time    seconds and year
    
#     Log To Console    ${time}
#     Log To Console    ${secs}
#     Log To Console    ${year}
#     Log To Console    ${yyyy} ${mm} ${dd}
#     Log To Console    ${time2}
#     Log To Console    ${y} ${s}

# TC006 get time
#     ${time}= 	Get Time 	1177654467                          # Time given as epoch seconds
#     ${secs}= 	Get Time 	sec    2007-04-27 09:14:27         # Time given as a timestamp
#     ${year}= 	Get Time 	year    NOW                         # The local time of execution
#     @{time2}= 	Get Time 	hour min sec    NOW + 1h 2min 3s    # 1h 2min 3s added to the local time
#     @{utc}= 	Get Time 	hour min sec    UTC                 # The UTC time of execution
#     ${hour}= 	Get Time 	hour    UTC - 1 hour                # 1h subtracted from the UTC time

#     Log To Console    ${time}
#     Log To Console    ${secs}
#     Log To Console    ${year}
#     Log To Console    ${time2}
#     Log To Console    ${utc}
#     Log To Console    ${hour}

# TC007 Sub-String
    # ${string}    Set Variable    RobotFramework
    # ${ignore_first}=    Get substring    ${string}    5
    # Log To Console    ${ignore_first}

    # ${string2}    Set Variable    RobotFramework    SCBTechX
    # Log To Console    ${string2}
    # Log To Console    ${string2}[1]
    # Should Be Equal As Strings    ${string2}[1]    SCBTechX

# TC008 Create List
#     @{list}=    Create List    a    b    c
#     ${scalar}=    Create List    a    b    c
#     ${ints}=    Create List    ${1}    ${2}    ${3}
#     Log To Console    ${list}
#     Log To Console    ${scalar}
#     Log To Console    ${ints}
#     Log To Console    ${dictionary}

# TC009 Documentation
#     [Documentation]    Test Documentation in TC09
#     Log To Console    ${TC09}

# #----------------------- scalar
# TC010 Example scalar
#     [Documentation]    Example 2 in TC
#     #EX 2
#     ${hi}    set variable    Hello, there
#     ${said}    set variable    I said, ${hi}
#     Log To Console    ${said}

#     #EX 3
#     ${var1}    ${var2}    Set Variable    Hello    world
#     Log To Console    ${var1}
#     Log To Console    ${var2}

#     #EX 4
#     @{list}    Set Variable    a    b
#     ${items1}    ${items2}    Set Variable    ${list}
#     Log To Console    ${items1}
#     Log To Console    ${items2}

# #----------------------- list
# TC011 Example list
#     @{}

# TC TEST Example
#     Log To Console    ${name}
#     Log To Console    ${name2}
#     Log To Console    ${nothing}
#     Log To Console    ${many}

#     Log To Console    ${list[2]}
#     Log To Console    ${list}[2]
#     Log To Console    ${dictionary['color']}

# TC012 List Example
#     @{list1}    Set Variable    12    123    345
#     Log To Console    ${list1}
#     ${count_list}=    Get Length    ${list1}
#     Log To Console    ${count_list}

#     Append To List    ${list1}    999
#     Log To Console    ${list1}
#     ${count_list}=    Get Length    ${list1}
#     Log To Console    ${count_list}

#     Log To Console    ${dictionary1}
#     ${count_dict}=    Get Length    ${dictionary1}
#     Log To Console    ${count_dict}

#     Set To Dictionary    ${dictionary1}    c=ccc
#     ${count_dict}=    Get Length    ${dictionary1}
#     Log To Console    ${count_dict}

#     Log Dictionary    ${dictionary1}
#     Log List    ${list1}

TC013 Calculate
    ${x}    Set Variable    7
    ${y}    Set Variable    15
    ${sum}    do plus    ${x}    ${y}
    # ${sum}=    Evaluate    ${x}*${y}
    # Log To Console    ${sum}

# TC014 if condition
#     ${x}    set variable    9
#     Run Keyword If    ${x}>9    Log To Console    X>9
#     ...    ELSE If    ${X}< and ${x}=9    Log To Console    X<=9
#     ...    ELSE    Log To Console    no one

# TC015 for loop
#     # for in for use kryword
#     ${sumall}    set variable    0
#     FOR    ${i}    IN RANGE    0    10
#         ${sum}    for loop i    ${i}
#         ${sumall}    Evaluate    ${sumall}+${sum}
#     END
#     Log To Console    ${sum}